"""You're going to write a binary search function.
You should use an iterative approach - meaning
using loops.
Your function should take two inputs:
a Python list to search through, and the value
you're searching for.
Assume the list only has distinct elements,
meaning there are no repeated values, and 
elements are in a strictly increasing order.
Return the index of value, or -1 if the value
doesn't exist in the list."""
def binary_search(input_array, value):
    """Your code goes here."""
    length = len(input_array)
    low = 0
    mid = 0
    high = length-1
    mid = (low+high)/2
    counter = 0
    found = 0
    while low<=high and not found:
        counter += 1
        print low, mid, high, input_array[mid]
        if input_array[mid] == value:
            found = 1
            return mid,counter
        else:
            if input_array[mid] < value:
                low = mid + 1
            else:
                high = mid - 1
        mid = (low+high)/2
    return -1,counter

test_list = range(0,16)
print test_list
test_val1 = 51
print binary_search(test_list, test_val1)