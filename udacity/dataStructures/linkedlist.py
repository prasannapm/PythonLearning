"""The LinkedList code from before is provided below.
Add three functions to the LinkedList.
"get_position" returns the element at a certain position.
The "insert" function will add an element to a particular
spot in the list.
"delete" will delete the first element with that
particular value.
Then, use "Test Run" and "Submit" to run the test cases
at the bottom."""

class Element(object):
    def __init__(self, value):
        self.value = value
        self.next = None
        
class LinkedList(object):
    def __init__(self, head=None):
        self.head = head
        
    def append(self, new_element):
        current = self.head
        if self.head:
            while current.next:
                current = current.next
            current.next = new_element
        else:
            self.head = new_element
            
    def print_list(self):
        current = self.head
        ll = []
        while current != None:
            ll.append(current.value)
            current = current.next
        print(ll)
            
    def get_position(self, position):
        """Get an element from a particular position.
        Assume the first position is "1".
        Return "None" if position is not in the list."""
        if position < 1:
            return None
        current = self.head
        counter = 1
        while current != None:
            if counter == position:
                return current
            else:
                current = current.next
                counter += 1
    
    def insert(self, new_element, position):
        """Insert a new node at the given position.
        Assume the first position is "1".
        Inserting at position 3 means between
        the 2nd and 3rd elements."""
        current = self.head
        temp = 1
        if position == 1:
            new_element.next = self.head.next
            self.head = new_element
            return
        while current != None:
            if temp == position-1:
                new_element.next = current.next
                current.next = new_element
                return
            else:
                current = current.next
                temp += 1
        pass
    
    
    def delete(self, value):
        """Delete the first node with a given value."""
        current = self.head
        if value == current .value and current == self.head:
            self.head = current.next
            return
        
        prev = self.head
        current = self.head.next
        while current != None:
            if value == current.value:
                prev.next = current.next
                return
            else:
                prev = prev.next
                current = current.next
        pass

# Test cases
# Set up some Elements
e1 = Element(1)
e2 = Element(2)
e3 = Element(3)
e4 = Element(4)

# Start setting up a LinkedList
ll = LinkedList(e1)
ll.append(e2)
ll.append(e3)

print("Initial list")
ll.print_list()
# Test get_position
# Should print 3
print(ll.head.next.next.value)
# Should also print 3
print(ll.get_position(3).value)

# Test insert
ll.insert(e4,3)
# Should print 4 now
print(ll.get_position(3).value)

print("After inserting 4 at position 3")
ll.print_list()

# Test delete
ll.delete(1)

print("After Deleting 4")
ll.print_list()
# Should print 2 now
print(ll.get_position(1).value)
# Should print 4 now
print(ll.get_position(2).value)
# Should print 3 now
print(ll.get_position(3).value)