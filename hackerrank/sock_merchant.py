# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

#!/bin/python3

# Complete the sockMerchant function below.
def sockMerchant(n, ar):
    l=[]
    count = 0
    for i in range(n):
        if ar[i] in l:
            l.remove(ar[i])
            count += 1
        else:
            l.append(ar[i])
    return count

if __name__ == '__main__':

    n = int(input())

    ar = list(map(int, input().rstrip().split()))

    result = sockMerchant(n, ar)

    print(str(result) + '\n')
