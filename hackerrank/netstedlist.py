#!env /usr/bin/python2

arr=[]
for _ in range(int(raw_input())):
    n = raw_input()
    s = float(raw_input())
    arr.append([n,s])

arr.sort(key=lambda x: x[1])
name = [i[0] for i in arr]
score = [i[1] for i in arr]
min_s=min(score)

while(score[0] == min_s):
    score.remove(score[0])
    name.remove(name[0])

newlist=[]
for i in range(len(score)):
    if score[i] == min(score):
        newlist.append(name[i])

l1=sorted(newlist)
for i in l1:
    print i