# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

#!/bin/python3

# Complete the sockMerchant function below.
def sockMerchant(n, ar):
    count = 0
    up = 0
    print(ar)
    for i in ar:
        if i == 'U':
            up += 1
        if i == 'D':
            up -= 1
        if up == 0 and i  == 'U':
            count += 1

    return count

if __name__ == '__main__':

    n = int(input())

    ar = input()

    result = sockMerchant(n, ar)

    print(str(result) + '\n')
