# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
from collections import Counter 

no_of_shoes=int(input())
shoes=Counter(map(int,input().split()))
no_of_cust=int(input())

tot_earned = 0

for i in range(no_of_cust):
    size,price = map(int,input().split())
    if shoes[size]:
        tot_earned += price
        shoes[size] -= 1

print(tot_earned)
        
    
    