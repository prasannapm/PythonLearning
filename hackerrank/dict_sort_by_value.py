#!env /usr/bin/python2


mydict = {'a':24,'g':52,'i':12,'k':33}

mylist = [['a', 24], ['g', 52], ['i', 12], ['k', 33]]

print(sorted(mydict.items(), key=lambda x: x[1]))

print(sorted(mydict.values()))

print(sorted(mylist, key=lambda x: x[1]))
