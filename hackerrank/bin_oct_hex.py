def print_formatted(number):
    # your code goes here
    wid = len(bin(n)[2:])
    for i in range(1,n+1):
        print("%s %s %s %s" % (str(i).rjust(wid,' '),oct(i)[2:].rjust(wid,' '),(hex(i)[2:]).upper().rjust(wid,' '),bin(i)[2:].rjust(wid,' ')))

if __name__ == '__main__':
    n = int(input())
    print_formatted(n)