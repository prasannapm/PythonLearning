
def gen_lower_alpha():
    alphas=''
    a=ord('a')
    for i in range(a,a+26):
        alphas += chr(i)
    return alphas

def gen_upper_alpha():
    alphas=''
    a = ord('A')
    for i in range(a,a+26):
        alphas += chr(i)
    return alphas

print(gen_lower_alpha())
print(gen_upper_alpha())