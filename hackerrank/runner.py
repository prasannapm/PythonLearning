
def main():
    n = int(input())
    arr = map(int, input().split())
    mydict = dict()
    myset = set()
    for i in arr:
        mydict[i] = arr.count(i)
        myset.add(i)

    print(mydict)
    print(mydict.keys())
    print(mydict.values())
    print(myset)

if __name__ == "__main__":
    main()
