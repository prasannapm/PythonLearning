import textwrap

def wrap(string, max_width):
    rem=len(string)
    i=0
    result = ''
    while rem >=0:
        result = result + string[i:i+max_width] + '\n'
        rem -=max_width
        i += max_width

    return result
if __name__ == '__main__':
    result = wrap('ABCDEFGHIJKLMNOPQRSTUX',5)
    print(result)