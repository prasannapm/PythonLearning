#!env /usr/bin/python2

if __name__ == "__main__":
    n=int(raw_input())
    student_marks={}
    for i in range(n):
        line=raw_input().split()
        name=line[0]
        scores=line[1:]
        student_marks[name] = map(float,scores)

    student=raw_input()
    print("%.2f" % round(sum(student_marks[student])/3.0,2))