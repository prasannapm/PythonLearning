from itertools import permutations

def cal_permutation(str,n):
    my_list=list(permutations(str,n))

    for i in sorted(my_list):
        print(''.join(i))

if __name__ == '__main__':
    str,num=input().split()
    cal_permutation(str,int(num))
