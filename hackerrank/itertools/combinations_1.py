from itertools import combinations

def cal_permutation(str,n):
    my_list=list(combinations(str,n))

    for i in my_list:
        print(''.join(i))

if __name__ == '__main__':
    str,num=input().split()
    for i in range(1,int(num)+1):
        cal_permutation(sorted(str),i)
