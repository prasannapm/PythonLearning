from itertools import product

def cartesian_product1(A,B):
    cp=list()
    for i in A:
        for j in B:
            print((i,j),end=' ')

def cartesian_product(A,B):
    print(*product(A,B))

if __name__ == '__main__':
    A = list(map(int, input().split()))
    B = list(map(int, input().split()))
    cartesian_product(A,B)