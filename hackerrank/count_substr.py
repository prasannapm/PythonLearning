
def count_substring(string, sub_string):
    index = 0
    count = 0
    for i in range(len(string)):
        index = string.find(sub_string,index)
        if index != -1:
            index += 1
            count += 1
    return count

print(count_substring("ABCDCDC","CDC"))

