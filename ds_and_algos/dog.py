class Dog:
    def __init__(self, name, month, day,year, speakText):
        self.name = name
        self.month = month
        self.day = day
        self.year = year
        self.speakText = speakText

    def speak(self):
        return self.speakText

    def getName(self):
        return self.name

    def changeBark(self, newbark):
        self.speakText = newbark

    def __add__(self, other):
        return Dog("Puppy of "+self.name+" and " + other.name, self.month, self.day, self.year, self.speakText + other.speakText)


def main():
    boyDog = Dog("dog1", 1, 9, 1983, "WOOF")
    girlDog = Dog("dog2", 1, 9, 1988, "WOOF")

    print(boyDog.getName())

    print(boyDog.changeBark("MEOW"))

    print(boyDog.speak())

    childDog = boyDog + girlDog

    print(childDog.getName())

if __name__ == "__main__":
    main()

