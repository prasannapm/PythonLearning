"""
brute force
duplicates are possible
time: O(n^3)
space: O(n)
"""

def threeSum(nums,n):
    l = []
    for i in range(0,n-2):
        for j in range(i+1,n-1):
            for k in range(j+1,n):
                if nums[i] + nums[j] + nums[k] == 0:
                    if [nums[i],nums[j],nums[k]] not in l:
                        l.append([nums[i],nums[j],nums[k]])
    return l

"""

"""
def threeSum2(arr,n):
    found = False
    for i in range(n - 1):
        s = set()
        #print(s)
        for j in range(i + 1, n):
            x = -(arr[i] + arr[j])
            if x in s:
                #print(x, arr[i], arr[j])
                found = True
            else:
                print(i,j,arr[j])
                s.add(arr[j])
    if found == False:
        print("No Triplet Found")

nums = [-1, 0, 1, 2, -1, -4]
#print(threeSum(nums,len(nums)))
#nums = [0, -1, 2, -3, 1]
print(threeSum2(nums,len(nums)))