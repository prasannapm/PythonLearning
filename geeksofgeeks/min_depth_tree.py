
# Find the minimum depth from root to leaf


class Node:
    def __init__(self, data=None):
        self.data = data
        self.left = None
        self.right = None


def minDepth(root):

    if root == None:
        return 0

    if root.left is None and root.right is None:
        return 1

    queue = [(root, 1)]

    while len(queue) > 0:
        print(queue)
        curr_node, curr_depth = queue.pop(0)
        if curr_node.left == None and curr_node.right == None:
            return curr_depth

        if curr_node.left is not None:
            queue.append((curr_node.left, curr_depth+1))
        if curr_node.right is not None:
            queue.append((curr_node.right, curr_depth+1))


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.right.right = Node(7)
root.right.right.left = Node(14)
root.right.right.right = Node(15)

print(minDepth(root))

