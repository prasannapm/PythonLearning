# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

#!/bin/python3

print('Please think of a number between 0 and 100!')

low = 0
high = 100
mid = int((low+high)/2)

while mid != low or mid != high:
    
    print('Is your secret number ', end='')
    print(mid)
    inp = input('Enter \'h\' to indicate the guess is too high. Enter \'l\' to indicate the guess is too low. Enter \'c\' to indicate I guessed correctly. ')
    if inp == 'l' or inp == 'h' or inp == 'c':
        if inp == 'l':
            low = mid
        elif inp == 'h':
            high = mid
        elif inp == 'c':
            print('Game over. Your secret number was: ',end='')
            print(mid)
            break
        mid = int((low+high)/2)
    else:
        print('Sorry, I did not understand your input.')
