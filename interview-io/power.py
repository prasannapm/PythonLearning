

def power(x,y):
    recp=0
    even=0
    if y == 0:
        return 1
    if x == 0:
        return 0
    if y < 0:
        recp=1
    if not x%2:
        even=1
    sum = 1
    if even:
        x=abs(x)
    y=abs(y)
    for i in range(y):
        sum = sum * x
    if recp:
        return 1.0/sum
    else:
        return sum

print("should print 81", power(3,4))
print("should print 0", power(0,4))
print("should print 2", power(2,1))
print("should print 1", power(3,0))
print("should print 0.5", power(2,-1))
print("should print 0.25", power(2,-2))
print("should print 4", power(-2,2))