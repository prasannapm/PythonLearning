#!env /usr/bin/python2

import math

def get_distance(p1,p2):
    return math.sqrt((p1[0]-p2[0])**2+(p1[1]-p2[1])**2)


def is_square(points):
    l = len(points)
    distance = set()
    distance.add(get_distance(points[0],points[1]))
    distance.add(get_distance(points[0], points[2]))
    distance.add(get_distance(points[0], points[3]))
    print distance
    return len(distance) == 2

if __name__ == '__main__':
    input=[[0,0],[0,1],[10,0],[1,1]]
    print is_square(input)

