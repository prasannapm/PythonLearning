#!env /usr/bin/python2

"""

[1,1,1,2,2,3] with k=2 is [1,2]
[1,1,1,1,2,2] with k=1 is [1]

"""

def k_most_frequent(arr,x):
    mydict={}
    mylist=[]
    for i in range(len(arr)):
        mydict[arr[i]]=mydict.get(arr[i],0) + 1
    print mydict
    for j,k in mydict.iteritems():
        print j,k
        if k >= x:
            mylist.append(j)
    return mylist


arr = [3, 1, 4, 4, 5, 2, 6, 1]
print k_most_frequent(arr,2)






