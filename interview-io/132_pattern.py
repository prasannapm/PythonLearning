#!env /usr/bin/python2

"""
1 3 2 pattern in an array

[10,50,25] = True
[10,20,30] = False

"""

def one_three_two_patern(arr):
    for i in range(len(arr)-2):
        for j in range(i+1,len(arr)-1):
            for k in range(j+1, len(arr)):
                if arr[j] > arr[i] and arr[j] > arr[k]:
                    if arr[i] < arr[k]:
                        print arr[i],arr[j],arr[k]
                        return True
    return False


arr=[10,20,30,40,50,35]

print one_three_two_patern(arr)


