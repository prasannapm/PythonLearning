#!env /usr/bin/python2

class Node:
    def __init__(self, data=None):
        self.data = data
        self.left = None
        self.right = None


# Find the heaviest weight path from root to leaf
# Return the weight itself and not the path

# Heaviest path: greatest sum of node data from root to leaf

#    3
#  /   \
# 2     1


def heaviestWeight(root):
    if root == None:
        return 0

    queue = [(root, root.data)]
    max_weight = None

    print len(queue)

    while len(queue) > 0:
        print queue
        curr_node, curr_weight = queue.pop(0)
        if curr_node.left == None and curr_node.right == None:
            if max_weight == None:
                max_weight = curr_weight
            elif curr_weight > max_weight:
                max_weight = curr_weight

        else:

            if curr_node.left != None:
                queue.append((curr_node.left, curr_weight + curr_node.left.data))

            if curr_node.right != None:
                queue.append((curr_node.right, curr_weight + curr_node.right.data))

    return max_weight


"""
       10
    5     20
  3  8  12  22
"""
root = Node(10)

root.right = Node(20)
root.left = Node(5)

root.right.left = Node(12)
root.right.right = Node(22)

root.left.left = Node(3)
root.left.right = Node(8)

print heaviestWeight(root)